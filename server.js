
const express = require('express'),
path = require('path'),
bodyParser = require('body-parser'),
cors = require('cors'),
mongoose = require('mongoose'),
// config = require('./config/DB'),
routesuser = require('./routes/api.routes');

var app = express();
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://gmc:gmcproject1@ds161950.mlab.com:61950/gmclogistics').then(
() => {console.log('Database is connected') },
err => { console.log('Can not connect to the database'+ err)}
);
// mongodb://varun:varun1@ds139883.mlab.com:39883/bsnl
// mongodb://gmc:gmcproject1@ds161950.mlab.com:61950/gmclogistics
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({'extended':'false'}));
// app.use(express.static(path.join(__dirname, 'src/assets')));

app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
app.use(express.static(path.join(__dirname, 'dist')));
// app.use('/login', express.static(path.join(__dirname, 'dist')));
app.use('/callapi', routesuser);

// console.log("PATHHHH",path.join(__dirname, 'uploads'))
app.use(express.static(path.join(__dirname, 'shipmentimport')));
// app.use(express.static(path.join(__dirname, 'signature')));
app.use(function(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PATCH, DELETE, OPTIONS');
  next();
});
console.log("dir_name",__dirname);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  // res.status(err.status || 500);
  // res.status(404).json({error:err});
  // res.redirect('/login');
});

module.exports = app;

