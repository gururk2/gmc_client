var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var androidSchema = new Schema({
  slot : {type : Array,default:''},

  created_by : {type: String,default:''},

  created_datetime : {type:Date,default:''},

  updated_by : {type:String,default:''},

  updated_datetime : {type:Date,default:''},

  FEid : {type:Array,default:''},

  FEname : {type:Array,default:''},

  allotstatus : {type:String,default:''},

  slotstatus : {type:String,default:''},

  statusid : {type:Number,default:''},

  statusstring : {type:Number,default:''},

  pincode : {type:Array,default:''},

  idslot : {type:Number,default:''},

  sortingstatus : {type:Boolean,default:false},

  warehousestatus:{type:String,default:''},

  pickedstatus : {type:String,default:''}
},
{
    collection:'shipmentdata'
});

module.exports = mongoose.model('Android', androidSchema);