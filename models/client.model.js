var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var productSchema = new Schema({

  AirWaybill_No: { type: Number,default:'' },

  Order_No:    { type: Number,default:''},

  Product_Type: { type: String,default:'' },

  Weight : {type : String,default:''},

  Vol_Weight : { type : String,default:''},

  COD_Amount: { type: Number,default:'' },

  Origin_Pincode : { type : Number,default:'' },

  Origin_SC : { type: String,default:''},

  Origin_City : { type : String,default:''},

  Destination_Pincode: { type: Number,default:'' },

  Destination_SC : {type:String,default:''},

  Destination_City : { type:String,default:''},

  Zone : { type : String,default:'' },

  Vendor : { type : String,default:'' },  

  Consignee : { type: String,default:''},

  Softdata_Upload : { type: String,default:''},

  Expected_Date : {type : String,default:''},

  Status : { type : String,default:'' },
  
  Remarks : {type:String ,default:''},
  
  Current_Reason : {type:String ,default:''},

  Summary_Status : {type:String,default:''},

  Reason : {type:String ,default:''},

  Last_Reason : {type:String ,default:''},

  Delivery_date : {type:String,default:''},

  Received_by : {type:String,default:''},
  
  Lastupdated_date : {type:String ,default:''},
  
  Return_status : {type:String ,default:''},

  First_outscan : {type:String ,default:''},

  Last_outscan : { type:String ,default:''},

  No_of_attempts : { type: String ,default:''},

  Company_name :  { type: String ,default:''},

  allot_status :  { type: String ,default:''},

  customer_name:{type:String,default:''},

  mobile:{type:Number,default:''},

  lat : { type:Number, default:'' },

  lng : { type:Number,default:'' },

  userid : { type : String,default:''},

  address : { type : String,default:''},
  
  location_updated : {type:String,default:''},

  signature : {type:String,default:''},

  prioritystatus : {type:String,default:''},

  deliverstatus : {type:Boolean,default:false},

  statusid : {type:String,default:''},
  
  isnew:{type:Boolean},

  iserror:{type:Boolean},

  created_datetime : {type:Date,default:Date.now()},

  updated_datetime : {type:Date,default:Date.now()},

  created_by : {type: String,default:''},

  updated_by : {type:String,default:''},

  Email:{type:String,default:''},


},
{
    collection:'Shipments'
});

module.exports = mongoose.model('Products', productSchema);