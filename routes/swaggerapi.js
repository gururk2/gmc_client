const express = require('express');
const router = express.Router();
const Client = require('../models/client.model');
const Clientlogin = require('../models/clientlogin.model');
const https = require('https');
const format = require('date-format');
const Cryptr = require('cryptr');
var localStorage = require('localStorage')
const cryptr = new Cryptr('myTotalySecretKey');
//////////////////////jwt globle declaraction
const jwt = require('jsonwebtoken');
var Regex = require("regex");
var regexp = require('node-regexp');
// var parse = require('csv-parse');
var parse = require('csv-parse/lib/sync');
//remove spacial charater https://www.npmjs.com/package/stripchar
///////////////////////// remove the spacila charater npm install stripchar
/////////////////////////stripchar.RSExceptUnsAlpNum('ne@w_t#est_id_@12');
var stripchar = require('stripchar').StripChar;

const multer = require('multer');


var app = express();


// var geocoder = require('geocoder');
var NodeGeocoder = require('node-geocoder');
 
var options = {
  provider: 'google',
 
  // Optional depending on the providers
  httpAdapter: 'https', // Default
  apiKey: 'AIzaSyC7uUtFA0Y9D2JM4DcY4TgVJ-yoO09spoM', // for Mapquest, OpenCage, Google Premier
  formatter: null         // 'gpx', 'string', ...
};
 
var geocoder = NodeGeocoder(options);

// const sortByDistance = require('sort-by-distance')
const sortByDistance =require('sort-by-distance');
var distance = require('google-distance-matrix');
//end of google api

  //storage1
  //npm install fast-csv fs
  var csv = require('fast-csv');
  var fs = require('fs');
  const storage = multer.diskStorage({
      destination: function(req, file, cb) {
        cb(null, './shipmentimport/');
      },
      filename: function(req, file, cb) {
  
        cb(null, new Date().toISOString() + file.originalname);
      }
    });12.972982, 77.511161

    
  const fileFilter = (req, file, cb) => {
    
    if (file.mimetype === 'image/jpeg'||file.mimetype === 'image/png'||file.mimetype === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
    ||file.mimetype === 'text/csv'||file.mimetype === 'text/plain'||file.mimetype === 'application/pdf') {
      cb(null, true);
    } else {
      cb(null, false);
    }
  };

  const upload = multer({
    storage: storage,
    limits: {
      fileSize: 1024 * 1024 * 6
    },
    fileFilter: fileFilter
  });



app.set('superSecret', 'ilovescotchyscotch');
////////////////////end of globle declaraction JWT

// const queryString = require('querystring' clientlogin.model)
// const url = require('url')

// router.get('/compdetails/:comp',function(req,res)
// {
//     var comp = req.params.comp;
//     console.log(comp);
//     Client.find({Company_name:comp},function(err,result1)
    
//     {
//         console.log(result1.length);
//         res.json(result1);
//     });


// });
router.get('/compdetails/:pagesize/:page/:comp/:date', (req, res, next) => {  
  
    // console.log(req.params);
    // console.log(req.params.pagesize)
    // console.log(req.params.page)
    // console.log(req.params.comp)
    const date = decodeURIComponent(req.params.date);
    // console.log(date);
    day = format.asString('dd/MM/yy',new Date(date));
    // console.log(day);
    const pageSize = parseInt(req.params.pagesize);
    const currentPage = parseInt(req.params.page);
    const comp = 'FK';
   // console.log(comp);
    const postQuery = Client.find({Company_name:comp,Softdata_Upload:day},'AirWaybill_No Lastupdated_date customer_name mobile Product_Type');
    // console.log(postQuery);
    let fetchedPosts;
    if (pageSize && currentPage) {
      postQuery.skip(pageSize * (currentPage - 1)).limit(pageSize);
    }
    postQuery
      .then(documents => {
        fetchedPosts = documents;
        // console.log(fetchedPosts);
        return Client.find({Company_name:comp,Softdata_Upload:day}).count();
      })
      .then(count => {
        res.status(200).json({
          message: "Posts fetched successfully!",
          posts: fetchedPosts,
          maxPosts: count
        });
      });
  });

// router.get('/table', (req, res, next) => {  
//     console.log(req.query);
//     const pageSize = +req.query.pagesize;
//     const currentPage = +req.query.page;
//     const postQuery = Client.find();
//     let fetchedPosts;
//     if (pageSize && currentPage) {
//       postQuery.skip(pageSize * (currentPage - 1)).limit(pageSize);
//     }
//     postQuery
//       .then(documents => {
//         fetchedPosts = documents;
//         return Client.count();
//       })
//       .then(count => {
//         res.status(200).json({
//           message: "Posts fetched successfully!",
//           posts: fetchedPosts,
//           maxPosts: count
//         });
//       });
//   });

router.get('/nopack/:comp',function(req,res)
{
    today = format.asString('dd/MM/yy',new Date());
    var comp = req.params.comp;
//     console.log(comp);
// console.log(today);
    Client.find({Company_name:comp,Softdata_Upload:today}).count().exec(function(err, result1){
        // console.log(result1);
        res.json(result1);
    });

});

router.get('/nopackalloted/:comp',function(req,res)
{
    today = format.asString('dd/MM/yy',new Date());
    var comp = req.params.comp;
    // console.log(comp);

    
    Client.find({Company_name:comp,allot_status:"Alloted",Softdata_Upload:today}).count().exec(function(err, result1){
        // console.log(result1);
        res.json(result1);
    });

});

router.get('/nopackallotedslots/:comp',function(req,res)
{
    today = format.asString('dd/MM/yy',new Date());
    var comp = req.params.comp;
    Client.aggregate([
        {$match:{Company_name:comp,Softdata_Upload:today}},
        // {$group:{_id:"$Destination_City",total:{$sum:1}}}
{$group:{_id:{Destination_City:"$Destination_City",allot_status:"$allot_status"},

total: { $sum: 1}
}}

        // { $group:
        //     { _id: { year: "$year", month: "$month" },
        //       sum: { $sum: "$amount" }
        //     } 



],function(err,res1){
    res.json(res1);
})

});



router.post('/getdateformate/',function(req,res)
{
    const comp = 'FK';
    // console.log(req.body);
    var date = req.body.date;
    // console.log(date);
    const nowdate = format.asString('dd/MM/yy',new Date(date));
    // const hi = new Date(date);
    // console.log(nowdate);

    Client.find({Company_name:comp,Softdata_Upload:nowdate},function(err,result1)
    
        {
            // console.log(result1.length);
            res.json(result1);
        });

});






router.post('/clientlogin',function(req,res)
{
//    console.log(req.body);
    var email = req.body.email;
    var password = req.body.password;
    // console.log(email);
    // console.log(password);
// console.log(today);
Clientlogin.findOne({name:email,password:password},function(err, result1){
        
        // demoid = cryptr.encrypt(result1.id);
        // console.log(demoid);
        //  ids = cryptr.decrypt(demoid);
        //  console.log(ids);
        // res.json(result1);
if(result1==undefined){
res.json({message:"Invalid User",status:false});
}else{



//////////////////////////////////////JWT section start
        // const payload = {
        //     admin: result1._id ,
        //     admin: result1.email 
        //   };
        //       var token = jwt.sign(payload, app.get('superSecret'), {
        //         expiresIn: "300" // expires in 24 hours
        //       });
        //       console.log(token);
        res.json({
            result1:result1,
            // token:token,

        //   demo:demoid,
            status:true,
        // expiresIn:300
    }
        );

///////////////////////////////end of JWT section
        }

});

});


function fullUrl(req) {
    return url.format({
      protocol: req.protocol,
      host: req.get('host'),
      pathname: req.originalUrl
    });
  }






  



router.post("/shipmentimport", upload.single('shipmentexcle'), (req, res, next) => {
    // console.log(req.file);

    // stripchar.RSExceptUnsAlpNum('ne@w_t#est_id_@12');

    // var records = parse(req, {columns: true});
    // var records =  csv.fromPath(req.file.path, {columns: true});
 

    let output;
    var  products  = []
        var csvStream = csv.fromPath(req.file.path, {headers: true})
            .on("data", function(data){
         
     
             var item = new Client(data);

              item.save(function(error,res3){
            
                  var add = stripchar.RSExceptUnsAlpNum(res3.address);
               
                  console.log(add);
                  https.get("https://maps.googleapis.com/maps/api/geocode/json?address="+add+"&key=AIzaSyC7uUtFA0Y9D2JM4DcY4TgVJ-yoO09spoM",(resp)=>{
        let data = '';
        resp.on('data', (chunk) => {
          data += chunk;
        });
      resp.on('end',()=>{
        var ab = JSON.parse(data);
        console.log(ab);
        var lat = ab.results[0].geometry.location.lat;
        var lng = ab.results[0].geometry.location.lng;
        console.log(lat,lng);
        Client.updateOne({"_id":res3._id},{$set:{"lat":lat,"lng":lng}}).exec(function(err,res4){
        
        })
      
      });
      });
    
              Client.updateMany({$set:{allot_status:"Not Alloted"}},function(err,res){
            
              })
              }); 
    
        }).on("end", function(){
          
          
        });
       
    })

module.exports = router;

