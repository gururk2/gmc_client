import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, PageEvent } from '@angular/material';
import { Client } from '../../../models/client.model';
import { CommonService } from 'app/common.service';
import { Router } from '@angular/router';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-exception',
  templateUrl: './exception.component.html',
  styleUrls: ['./exception.component.scss']
})
export class ExceptionComponent implements OnInit {



  displayedColumns: string[] = ['AirWaybill_No', 'Order_No','customer_name','mobile','Email','address'];
  
  dataSource;
  coins:any;
  obj:any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  isLoading = false;
  nowdate:any;
  posts:Client[] = [];
  totalPosts = 0;
  postsPerPage = 15;
  currentPage = 1;
  pageSizeOptions = [500,1000,1500,2000,2500,3000];
  comp=16;
  ddd;
  mmm;
  constructor(private service:CommonService,private router:Router,fb: FormBuilder){}
  
  selectedFile:File = null;



//end datepicker
ngOnInit() {
  console.log("teseses")
 var datenow = new Date();
 console.log(datenow);
 var dd = datenow.getDate();
 var mm = datenow.getMonth()+1;
 var yy = datenow.getFullYear();
 console.log(dd);
 console.log(mm);
 console.log(yy);
 if(dd<10 && mm<10) {
   this.ddd = '0'+dd;
   this.mmm = '0'+mm;
   this.nowdate = this.mmm + '/' + this.ddd + '/' + yy;
} 
else if(dd<10 && mm>9) {

   this.ddd = '0'+dd;
   this.nowdate = mm + '/' + this.ddd + '/' + yy;
}
else if(dd>9 && mm<10) {
 this.mmm = '0'+mm;
 this.nowdate = this.mmm + '/' + dd + '/' + yy;
}else{
 this.nowdate = mm + '/' + dd + '/' + yy;
}


   // this.coins = Date.now();
   // console.log(this.coins);
   console.log(this.nowdate);
   this.isLoading = true;
   this.service.getException(this.postsPerPage, this.currentPage,this.comp,this.nowdate);
   this.service.getshipment().subscribe((postData: {posts: Client[], postCount: number})=> {
     this.isLoading = false;
       this.totalPosts = postData.postCount;
       this.dataSource = new MatTableDataSource(postData.posts);
     // this.coins = res;
     console.log(postData.posts);
     console.log(postData.postCount);
     this.dataSource.sort = this.sort;
 });
   // this.dataSource.paginator = this.paginator;
}



orgValueChange(date){
  console.log(date);
  // this.obj = {
  //   date : date
  // }
  this.nowdate = date;
  // this.service.getdateformate(this.obj).subscribe(res=>{
    //   console.log(res);
    this.isLoading = true;
    this.service.getException(this.postsPerPage, this.currentPage,this.comp,this.nowdate);
  console.log("hii");
// });

}
  applyFilter(filterValue: string) {
    console.log(filterValue);
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  onChangedPage(pageData: PageEvent) {
    this.isLoading = true;
    this.currentPage = pageData.pageIndex + 1;
    this.postsPerPage = pageData.pageSize;
    this.service.getException(this.postsPerPage, this.currentPage,this.comp,this.nowdate);
  }

  onFileSelected(event)
  {
  console.log(event);
  console.log(event);
  this.selectedFile = <File>event.target.files[0];
  
  
  }

  press() {
  
    this.router.navigate(['/clientlist']);
    }

  logout(){

   
    localStorage.clear();

    window.location.reload();
  }

}
