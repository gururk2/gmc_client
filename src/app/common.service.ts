import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { httpFactory } from '@angular/http/src/http_module';
import { Client } from '../../models/client.model';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import {Idle, DEFAULT_INTERRUPTSOURCES} from '@ng-idle/core';
import {Keepalive} from '@ng-idle/keepalive';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
    private posts: Client[] = [];
    //jwt globle variable
    
     private token: String;
     private isAuthenticated=false;
     private tokenTimer: any;
     private authStatusListener = new Subject<boolean>();
     getToken() {
      return this.token;
    }
  
    getIsAuth() {
      return this.timedOut;
    }
  
    getAuthStatusListener() {
      return this.authStatusListener.asObservable();
    }
  
//jwt end globle varible

idleState = 'Not started.';

timedOut = false;

lastPing?: Date = null;

    private postsUpdated = new Subject<{ posts: Client[]; postCount: number }>();
    constructor(private idle: Idle, private keepalive: Keepalive,
      private http:HttpClient,private service:CommonService,private router:Router,private cookie:CookieService){

        idle.setIdle(500);
        // sets a timeout period of 5 seconds. after 10 seconds of inactivity, the user will be considered timed out.
        idle.setTimeout(5);
        // sets the default interrupts, in this case, things like clicks, scrolls, touches to the document
        idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
    
        idle.onIdleEnd.subscribe(() => this.idleState = 'No longer idle.');
        idle.onTimeout.subscribe(() => {
          this.idleState = 'Timed out!';
          this.timedOut = true;
          localStorage.clear();
    
    
          // this.router.navigate(['/login']);
          window.location.reload();
      
        });
        idle.onIdleStart.subscribe(() => this.idleState = 'You\'ve gone idle!');
        idle.onTimeoutWarning.subscribe((countdown) =>
        {
           this.idleState = 'You will time out in ' + countdown + ' seconds!';
        // 'You will time out in ' + countdown + ' seconds!';
      });
        // sets the ping interval to 15 seconds
        keepalive.interval(15);
    
        keepalive.onPing.subscribe(() => this.lastPing = new Date());
    
        this.reset();
      }
    
      reset() {
        this.idle.watch();
        this.idleState = 'Started.';
        this.timedOut = false;
      }
 
    getcompdetails(comp)
    {
        console.log(comp);
        const uri = "http://localhost:4002/callapi/compdetails/" + comp;
        return this.http.get(uri);
    }
    getnopack(comp)
    {
        console.log(comp);
        const uri = "http://localhost:4002/callapi/nopack/" + comp;
        return this.http.get(uri);
    }
    getnopackalloted(comp)
    {
        console.log(comp);
        const uri = "http://localhost:4002/callapi/nopackalloted/" + comp;
        return this.http.get(uri);
    }
    getnopackallotedslots(comp)
    {
        console.log(comp);
        const uri = "http://localhost:4002/callapi/nopackallotedslots/" + comp;
        return this.http.get(uri);
    }
    getshipment(){
     
        return this.postsUpdated.asObservable();
      }

      getPosts(postsPerPage: number, currentPage: number,comp,nowdate){
     
        let date = encodeURIComponent(nowdate);
        console.log(date);
        const queryParams = `/${postsPerPage}/${currentPage}/${comp}/${date}`;
        this.http
          .get<{ message: string; posts: any; maxPosts: number }>(
            "http://localhost:4002/callapi/compdetails" + queryParams
          )
          .pipe(
            map(postData => {
              console.log(postData.posts);
              return {
                posts: postData.posts,
               
                maxPosts: postData.maxPosts
              };
            })
          )
          .subscribe(transformedPostData => {
            this.posts = transformedPostData.posts;
            this.postsUpdated.next({
              posts: [...this.posts],
              postCount: transformedPostData.maxPosts
            });
          });
      }


      getdateformate(obj)
      {
          console.log(obj);
          const uri = "http://localhost:4002/callapi/getdateformate/";
          return this.http.post(uri,obj);
      }

      ///////////////////////////// jwt authentication  start
      login(obj)
      {
        // console.log(obj);
        const uri = "http://localhost:4002/callapi/clientlogin/";
        // return this.http.post(uri,obj);
        this.http.post(uri,obj).subscribe(res=>{
          // console.log(res[0].admin);

console.log(res);
          if(res['status']==false)
          {
            alert("Invalid Email Or Password");
        
            window.location.reload();
            
          }
  else{

    localStorage.setItem('id',res['result1']['_id']);
    // this.cookie.set('id',res['result1']['_id']);
    // localStorage.setItem('demo',res['demo']);
    this.router.navigate(["/dashboard"]);

     
    
}
        })
      }



      autoAuthUser() {
        const authInformation = this.getAuthData();
        if (!authInformation) {
          return;
        }
        const now = new Date();
        const expiresIn = authInformation.expirationDate.getTime() - now.getTime();
        if (expiresIn > 0) {
          this.token = authInformation.token;
          this.isAuthenticated = true;
       
          this.authStatusListener.next(true);
        }
      }
    
      logout() {
        this.token = null;
        this.isAuthenticated = false;
        this.authStatusListener.next(false);
        clearTimeout(this.tokenTimer);
    
        this.router.navigate(["/"]);
      }
    
      private setAuthTimer(duration: number) {
        console.log("Setting timer: " + duration);
        this.tokenTimer = setTimeout(() => {
          this.logout();
        }, duration * 1000);
      }
    
      // private saveAuthData(token: string, expirationDate: Date) {
      //   localStorage.setItem("token", token);
      //   localStorage.setItem("expiration", expirationDate.toISOString());
      // }
    
      // private clearAuthData() {
      //   localStorage.removeItem("token");
      //   localStorage.removeItem("expiration");
       

      // }
    
      private getAuthData() {
        const token = localStorage.getItem("token");
        const expirationDate = localStorage.getItem("expiration");
        if (!token || !expirationDate) {
          return;
        }
        return {
          token: token,
          expirationDate: new Date(expirationDate)
        }

      }


///////////////////////end jwt authentication 


importshipment(fdobject)
{
  console.log(fdobject);
  const uri = "http://localhost:4002/callapi/shipmentimport/"
    console.log(fdobject);
  return  this.http.post(uri,fdobject);
      // var a=res;
     
      // alert(a); a=res;
     
      // alert(a);
      // alert(JSON.stringify(res.json));
     
}



statustrack(airWaybillno)
{
  console.log(airWaybillno);

  const uri = "http://localhost:4002/callapi/statustrack/" +airWaybillno;
  return this.http.get(uri);
   

}

getException(postsPerPage: number, currentPage: number,comp,nowdate){
     
  let date = encodeURIComponent(nowdate);
  console.log(date);
  const queryParams = `/${postsPerPage}/${currentPage}/${comp}/${date}`;
  this.http
    .get<{ message: string; posts: any; maxPosts: number }>(
      "http://localhost:4002/callapi/exceptionlist" + queryParams
    )
    .pipe(
      map(postData => {
        console.log(postData.posts);
        return {
          posts: postData.posts,
         
          maxPosts: postData.maxPosts
        };
      })
    )
    .subscribe(transformedPostData => {
      this.posts = transformedPostData.posts;
      this.postsUpdated.next({
        posts: [...this.posts],
        postCount: transformedPostData.maxPosts
      });
    });
}





    }



