import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { CommonService } from './common.service';
// import { AuthService } from "./auth.service";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private service: CommonService,private router:Router){}
//   canActivate(
//     next: ActivatedRouteSnapshot,
//     state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
// this.router.navigate(['/']);

//     return false;
//   }


  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean | Observable<boolean> | Promise<boolean> {
    const isAuth = localStorage.getItem('id');
    if(isAuth!=null){
      return true;
    }else{
      this.router.navigate(['/login']);
      return false;
    }
    // console.log(isAuth);
    // if (!isAuth) {
    //   this.router.navigate(['/login']);
    // }
    // return isAuth;
  }



}
