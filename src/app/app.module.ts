import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import {DataTableModule} from "angular-6-datatable";
 

import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';

import { AppComponent } from './app.component';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { MatTableModule, MatPaginatorModule, MatSortModule, MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule } from '@angular/material';
import { CdkTableModule, DataSource } from '@angular/cdk/table';


import { HttpClient, HttpClientModule } from '@angular/common/http';

import { NgIdleKeepaliveModule } from '@ng-idle/keepalive';
import { ModalModule } from 'ngx-bootstrap/modal';

import { LayoutModule } from '@angular/cdk/layout';
import { CookieService } from 'ngx-cookie-service';
// import { ExceptionComponent } from './exception/exception.component';

@NgModule({
  exports:[
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    CdkTableModule,
    DataTableModule,
    
    
  ],
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    ComponentsModule,
    RouterModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    CdkTableModule,
  
    DataTableModule,
   
    AppRoutingModule,
    NgIdleKeepaliveModule.forRoot(),
    ModalModule.forRoot(),
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule
    
  ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    
  
 
    
    

  ],
  providers: [CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
