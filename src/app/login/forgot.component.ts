import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-login',
  templateUrl: './forgot.component.html',
  styleUrls: ['./login.component.scss']
})
export class ForgotComponent implements OnInit {
	registerForm: FormGroup;
	submittedError;
  constructor(private service:CommonService,private router:Router,private formBuilder:FormBuilder ) { }
//   save(email,pwd) {
//     console.log(email);
//     console.log(pwd);
//     this.router.navigate(['/dashboard']);
   

    
// }
  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]
    });
  }
  obj:any;
  login(form){
	
	if (this.registerForm.invalid) {
	  console.log("hii");
	  this.submittedError = true;
	  return;
  }
  this.submittedError = false;

  console.log(this.registerForm.value);
	this.obj={
	  email:this.registerForm.value.email,
	  password:this.registerForm.value.password
	}

  this.service.login(this.obj);
  
  }

}

