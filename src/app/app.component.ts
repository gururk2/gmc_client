import { Component} from '@angular/core';
import { CommonService } from './common.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private service:CommonService){}
ngOnInit(){
  //jwt
  this.service.autoAuthUser();
  //jwt reload
}
}
