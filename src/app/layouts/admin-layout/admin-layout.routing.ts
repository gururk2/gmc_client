import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';

import { LoginComponent } from '../../login/login.component';
import { ForgotComponent } from '../../login/forgot.component';

import { ClientlistComponent } from '../../clientlist/clientlist.component';
import { AuthGuard } from '../../auth.guard';
import { ShipmentComponent } from '../../shipment/shipment.component';

import { ExceptionComponent } from '../../exception/exception.component';

export const AdminLayoutRoutes: Routes = [

    { path: 'dashboard',    canActivate:[AuthGuard],    component: DashboardComponent },
 
    { path: 'forgot',          component: ForgotComponent },
    { path: 'login',          component:  LoginComponent},
    { path: 'clientlist',  canActivate:[AuthGuard]  ,component: ClientlistComponent },
    { path: 'shipment',     canActivate:[AuthGuard],component: ShipmentComponent },
    { path: 'exception',     canActivate:[AuthGuard],component: ExceptionComponent },
    
    
];
