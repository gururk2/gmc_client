import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../dashboard/dashboard.component';
// import { UserProfileComponent } from '../../user-profile/user-profile.component';
// import { TableListComponent } from '../../table-list/table-list.component';
// import { TypographyComponent } from '../../typography/typography.component';
// import { IconsComponent } from '../../icons/icons.component';
// import { MapsComponent } from '../../maps/maps.component';
// import { NotificationsComponent } from '../../notifications/notifications.component';
// import { UpgradeComponent } from '../../upgrade/upgrade.component';
import { LoginComponent } from '../../login/login.component';
import { ShipmentComponent } from '../../shipment/shipment.component';
import { ModalModule } from 'ngx-bootstrap/modal'; 
import { ClientlistComponent } from '../../clientlist/clientlist.component';
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive'; // this includes the core NgIdleModule but includes keepalive providers for easy wireup
import { ForgotComponent } from '../../login/forgot.component';
import { MomentModule } from 'angular2-moment'; // optional, provides moment-style pipes for date formattin
import {
  MatButtonModule,
  MatInputModule,
  MatRippleModule,
  MatTooltipModule,
  MatTableModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSortModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatCardModule,
  MatMenuModule,

} from '@angular/material';
import { CdkTableModule } from '@angular/cdk/table';
import PerfectScrollbar from 'perfect-scrollbar';
import { CookieService } from 'ngx-cookie-service';
import { ExceptionComponent } from 'app/exception/exception.component';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    CdkTableModule,
    MatNativeDateModule,
    MatMenuModule,
    MatTooltipModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatDatepickerModule,
    MomentModule,
    NgIdleKeepaliveModule.forRoot(),
    ModalModule.forRoot(),
    
    
  ],
  declarations: [
    DashboardComponent,
    // UserProfileComponent,
    // TableListComponent,
    // TypographyComponent,
    // IconsComponent,
    // MapsComponent,
    // NotificationsComponent,
    // UpgradeComponent,

    LoginComponent,
    ClientlistComponent,
    ForgotComponent,
    ShipmentComponent,
    ExceptionComponent
  ],
  providers: [CookieService],
})

export class AdminLayoutModule {}
