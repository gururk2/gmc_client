import { Component, OnInit ,ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from '../common.service';
import { PageEvent, MatTableDataSource, MatPaginator, MatSort, MatDatepickerInputEvent } from '@angular/material';
import { Client } from '../../../models/client.model';
import { DatePipe } from '@angular/common';
import { FormControl } from '@angular/forms';
@Component({
  selector: 'app-clientlist',
  templateUrl: './clientlist.component.html',
  styleUrls: ['./clientlist.component.scss']
})
export class ClientlistComponent implements OnInit{
  // fetchData:any;
  // constructor(private router:Router , private service:CommonService) { }
 
  // date = new FormControl(new Date());
  // console.log(date);
  // events: string[] = [];

//   addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
//     this.events.push(`${type}: ${event.value}`);
//     //console.log(this.events);
// const date=this.events;
// console.log(date);
//  this.datePipe.transform(myDate, 'yyyy-MM-dd'); 
//  console.log(date,"yyyy-MM-dd");
    // this.service.getdateformate(date).subscribe(res=>{
    //   console.log(res);
    
    // });


  // }



  displayedColumns: string[] = ['AirWaybill_No', 'Lastupdated_date', 'customer_name','Product_Type','mobile','Status'];
  res2='';
  picked='';
  warehouse1='';
  dataSource;
  coins:any;
  obj:any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  isLoading = false;
  nowdate:any;
  posts:Client[] = [];
  totalPosts = 0;
  postsPerPage = 15;
  currentPage = 1;
  pageSizeOptions = [500,1000,1500,2000,2500,3000];
  comp=16;
  ddd;
  mmm;
  constructor(private service:CommonService,private router:Router){}
  
  selectedFile:File = null;
  //datepicker 

//end datepicker
  ngOnInit() {
   //console.log("teseses")
  var datenow = new Date();
  console.log(datenow);
  var dd = datenow.getDate();
  var mm = datenow.getMonth()+1;
  var yy = datenow.getFullYear();
  console.log(dd);
  console.log(mm);
  console.log(yy);
  if(dd<10 && mm<10) {
    this.ddd = '0'+dd;
    this.mmm = '0'+mm;
    this.nowdate = this.mmm + '/' + this.ddd + '/' + yy;
} 
else if(dd<10 && mm>9) {

    this.ddd = '0'+dd;
    this.nowdate = mm + '/' + this.ddd + '/' + yy;
}
else if(dd>9 && mm<10) {
  this.mmm = '0'+mm;
  this.nowdate = this.mmm + '/' + dd + '/' + yy;
}else{
  this.nowdate = mm + '/' + dd + '/' + yy;
}


    // this.coins = Date.now();
    // console.log(this.coins);
    console.log(this.nowdate);
    this.isLoading = true;
    this.service.getPosts(this.postsPerPage, this.currentPage,this.comp,this.nowdate);
    this.service.getshipment().subscribe((postData: {posts: Client[], postCount: number})=> {
      this.isLoading = false;
        this.totalPosts = postData.postCount;
        this.dataSource = new MatTableDataSource(postData.posts);
      // this.coins = res;
      console.log(postData.posts);
      console.log(postData.postCount);
      this.dataSource.sort = this.sort;
  });
    // this.dataSource.paginator = this.paginator;
}
orgValueChange(date){
  console.log(date);
  // this.obj = {
  //   date : date
  // }
  this.nowdate = date;
  // this.service.getdateformate(this.obj).subscribe(res=>{
    //   console.log(res);
    this.isLoading = true;
    this.service.getPosts(this.postsPerPage, this.currentPage,this.comp,this.nowdate);
  console.log("hii");
// });

}
  applyFilter(filterValue: string) {
    console.log(filterValue);
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  onChangedPage(pageData: PageEvent) {
    this.isLoading = true;
    this.currentPage = pageData.pageIndex + 1;
    this.postsPerPage = pageData.pageSize;
    this.service.getPosts(this.postsPerPage, this.currentPage,this.comp,this.nowdate);
  }

  onFileSelected(event)
  {
  console.log(event);
  console.log(event);
  this.selectedFile = <File>event.target.files[0];
  
  
  }
  // obj:any;
  importshipment()
  {
    const fd=new FormData();
    fd.append('shipmentexcle',this.selectedFile,this.selectedFile.name);
    console.log("jjjjjj");
    console.log(fd);
    this.service.importshipment(fd).subscribe(res => {
      console.log(res)

if(res=="Insert")
{
alert("Insert Dtata");
}
else{
  alert("Invalid format");

}
  });
    }
press()
{
  this.router.navigate(['/dashboard']);
   
}
logout(){

   
  localStorage.clear();

  window.location.reload();
}


refresh()
{
  window.location.reload();
}




trackstatus(event)
{
  console.log("hai");
  var airWaybillno=event;
  console.log(airWaybillno);
if(airWaybillno!=null)
{

this.service.statustrack(airWaybillno).subscribe(res=>{
  console.log(res);
  if(res['result1']!=''){
  this.res2 = res['result1'][0].allotstatus;
  this.picked = res['result1'][0].pickedstatus;
  this.warehouse1 = res['result1'][0].warehousestatus;
  }
});


}
else{
alert("Enter AirWaybill No");
}




}


}
















// ngOnInit() {
//   let comp="FK";
//   this.service.getcompdetails(comp).subscribe(res=>{
//     console.log(res);
//     this.fetchData = res;
//     });

//  } 



