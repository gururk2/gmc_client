import { Component, OnInit, ElementRef } from '@angular/core';
import * as Chartist from 'chartist';
import * as Canvasjs from './canvasjs.min';


import { Router } from '@angular/router';
import { CommonService } from '../common.service';
import { ROUTES } from '../components/sidebar/sidebar.component';

import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  fetchData:any;
  fetchData2:any;
  fetchData3:any;

  private listTitles: any[];
  location: Location;
    mobile_menu_visible: any = 0;
  private toggleButton: any;
  private sidebarVisible: boolean;

  constructor(private router:Router,private service:CommonService,location: Location,  private element: ElementRef) { 
    this.location = location;
    this.sidebarVisible = false;
  }
 
  // nextpage() {
  //   this.router.navigate(['/typography']);

   
  //   }
  data = [];

  ngOnInit() {
//nav bar
this.listTitles = ROUTES.filter(listTitle => listTitle);
    const navbar: HTMLElement = this.element.nativeElement;
    this.toggleButton = navbar.getElementsByClassName('navbar-toggler')[0];
//No of Package

    let comp="FK";
    this.service.getnopack(comp).subscribe(res=>{
      console.log(res);
      this.fetchData = res;
    });

    //No of package Allocated
    this.service.getnopackalloted(comp).subscribe(res=>{
      console.log(res);
      this.fetchData2 = res;
    });

    //No Of Packages slot
    this.service.getnopackallotedslots(comp).subscribe(res=>{
      console.log(res);
      this.fetchData3 = res;
//end of no packages slot
      //pie chart no packages slot
      for(var i=0;i<this.fetchData3.length;i++){
        this.data.push({
          "y":this.fetchData3[i].total,
        "name":this.fetchData3[i]._id.Destination_City
        })
      }
      console.log(this.data);
      let chart = new Canvasjs.Chart("chartContainer", {
        theme: "light",
        animationEnabled: true,
        exportEnabled: true,
        title:{
          text: "Package Details"
        },
        data: [{
          type: "pie",
          showInLegend: true,
          toolTipContent: "<b>{name}</b>: {y} (#percent%)",
          indexLabel: "{name} - #percent%",
          dataPoints: this.data
       
        }
      ]

      });
        
      chart.render();
    });

  }
  getTitle(){
    var titlee = this.location.prepareExternalUrl(this.location.path());
    if(titlee.charAt(0) === '#'){
        titlee = titlee.slice( 2 );
    }
    titlee = titlee.split('/').pop();

    for(var item = 0; item < this.listTitles.length; item++){
        if(this.listTitles[item].path === titlee){
            return this.listTitles[item].title;
        }
    }
    return 'Dashboard';
  }

  press() {
  
  this.router.navigate(['/clientlist']);
  }

  Exception(){

    this.router.navigate(['/exception']);
  }

  shipment() {
  
    this.router.navigate(['/shipment']);
    }


  logout(){

   
    localStorage.clear();

    window.location.reload();
  }

}
